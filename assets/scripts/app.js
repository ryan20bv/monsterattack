const ATTACK_VALUE = 10; //global value
const STRONG_ATTACK_VALUE = 17; //global value
const MONSTER_ATTACK_VALUE = 14;
const HEAL_VALUE = 20;
const MODE_NORMAL_ATTACK = "Attack";
const MODE_STRONG_ATTACK = "Strong Attack"
const LOG_PLAYER_NORMAL_ATTACK = "Player Normal Attack";
const LOG_PLAYER_STRONG_ATTACK = "Player Strong Attack";
const LOG_PLAYER_HEAL = "Heal Player";
const LOG_MONSTER_ATTACK = "Monster Attack";
const LOG_LIFE_USED = "CHANCE IS USED";
const LOG_GAME_RESULT = "Game RESULT";


let enteredNum = prompt('Max life', '100');


let chosenMaxLife = parseInt(enteredNum);
if(isNaN(enteredNum) || enteredNum <= 0){
    chosenMaxLife = 100;
}
let currentMonsterHealth = chosenMaxLife;
let currentPlayerHealth = chosenMaxLife;
let hasBonusLife = true;
let isAbleBtn = true;
let battleLog = [];


adjustHealthBars(chosenMaxLife);

// write to log
function writeToLog(
    ev, 
    val, 
    monsterHealth, 
    playerHealth,
    ){
    let logEntry = {
        event: ev,
        value: val,
        finalMonsterHealth: monsterHealth,
        finalPlayerHealth: playerHealth,
    }
    if ( ev === LOG_PLAYER_NORMAL_ATTACK){
        logEntry.target = "Monster"
    } else if (ev === LOG_PLAYER_STRONG_ATTACK){
        logEntry.target = "Monster"
    } else if (ev === LOG_MONSTER_ATTACK){
        logEntry.target = "Player"
    } else if (ev === LOG_PLAYER_HEAL){
        logEntry = {
            event: ev,
            value: val,
            target: "Player",
            finalMonsterHealth: monsterHealth,
            finalPlayerHealth: playerHealth,
        }
    } else if (LOG_GAME_RESULT){
        logEntry = {
            event: ev,
            value: val,
            finalMonsterHealth: monsterHealth,
            finalPlayerHealth: playerHealth,
        }
    }
    battleLog.push(logEntry)
}

// reset game
function reset(){
    enteredNum = prompt('Max life', '100');
    chosenMaxLife = parseInt(enteredNum);
    currentMonsterHealth = chosenMaxLife;
    currentPlayerHealth = chosenMaxLife;
    hasBonusLife = true;
    resetGame(chosenMaxLife);
    disableBtn(!isAbleBtn);
    battleLog = [];
}

// button disabled after the game result
function disableBtn(isAbleBtn){
    attackBtn.disabled = isAbleBtn;
    strongAttackBtn.disabled = isAbleBtn;
    healBtn.disabled = isAbleBtn;
    
}

// each and after round
function endRound(){
    const initialLife = currentPlayerHealth;
    const damageToPlayer = dealPlayerDamage(MONSTER_ATTACK_VALUE);
    currentPlayerHealth -= damageToPlayer;
    writeToLog (
        LOG_MONSTER_ATTACK,
        damageToPlayer,
        currentMonsterHealth,
        currentPlayerHealth,
    )
    if(currentPlayerHealth <= 0 && hasBonusLife){
        hasBonusLife = false;
        bonusLifeEl.innerText = 0;
        currentPlayerHealth = initialLife;
        setPlayerHealth(currentPlayerHealth);
        alert("You were given a chance to attack");
        writeToLog(
            LOG_LIFE_USED,
            "Chance is Used",
            currentMonsterHealth,
            currentPlayerHealth
        )
    }

    if(currentMonsterHealth <= 0 && currentPlayerHealth > 0){
        alert("You Won!");
        disableBtn(isAbleBtn);
        writeToLog(
            LOG_GAME_RESULT,
            "Player Won",
            currentMonsterHealth,
            currentPlayerHealth
        )

    } else if(currentPlayerHealth <= 0 && currentMonsterHealth > 0){
        alert("You Lose!")
        disableBtn(isAbleBtn);
        writeToLog(
            LOG_GAME_RESULT,
            "Monster Won",
            currentMonsterHealth,
            currentPlayerHealth
        )
    } else if(currentMonsterHealth <= 0 && currentPlayerHealth <= 0){
        alert("It's a draw")
        disableBtn(isAbleBtn);
        writeToLog(
            LOG_GAME_RESULT,
            "A Draw",
            currentMonsterHealth,
            currentPlayerHealth
        )
    }
    // console.log(monsterHealthBar.value, playerHealthBar.value)
    // console.log(currentMonsterHealth, currentPlayerHealth)

}

// attack mode
function attackMode(mode){
    /* let maxDamage;
    let logEvent;
    if (mode === MODE_NORMAL_ATTACK){
        maxDamage = ATTACK_VALUE;
        logEvent = LOG_PLAYER_NORMAL_ATTACK;
    } else if (mode === MODE_STRONG_ATTACK){
        maxDamage = STRONG_ATTACK_VALUE;
        logEvent = LOG_PLAYER_STRONG_ATTACK;
    } */

    //  ternary operator

    let maxDamage = mode === MODE_NORMAL_ATTACK 
        ? ATTACK_VALUE 
        : STRONG_ATTACK_VALUE;
    
    let logEvent = mode === MODE_NORMAL_ATTACK 
        ? LOG_PLAYER_NORMAL_ATTACK
        : LOG_PLAYER_STRONG_ATTACK;

    const damageToMonster = dealMonsterDamage(maxDamage);
    currentMonsterHealth -= damageToMonster;
    writeToLog(
        logEvent,
        damageToMonster,
        currentMonsterHealth,
        currentPlayerHealth
    )
    endRound();
}

function attackHandler(){
    attackMode(MODE_NORMAL_ATTACK);
}

function strongAttackHandler(){
    attackMode(MODE_STRONG_ATTACK);
}

function healPlayerHandler(){
    let healValue;
    if (currentPlayerHealth >= chosenMaxLife - HEAL_VALUE){
        alert("You cant heal more than your max life")
        healValue = chosenMaxLife - currentPlayerHealth;
    } else {
        healValue = HEAL_VALUE
    }
    increasePlayerHealth(healValue);
    currentPlayerHealth += healValue;
    writeToLog(
        LOG_PLAYER_HEAL,
        healValue,
        currentMonsterHealth,
        currentPlayerHealth
    )
    endRound();
}

function logBattle (){
    console.log(battleLog)
}

attackBtn.addEventListener('click', attackHandler);
strongAttackBtn.addEventListener('click', strongAttackHandler);
healBtn.addEventListener('click', healPlayerHandler);
resetBtn.addEventListener('click', reset);
logBtn.addEventListener('click', logBattle);